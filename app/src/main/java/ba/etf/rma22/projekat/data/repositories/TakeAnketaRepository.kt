package ba.etf.rma22.projekat.data.repositories

import android.content.Context
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.api.ApiAdapter
import ba.etf.rma22.projekat.data.models.AnketaTaken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object TakeAnketaRepository {
    suspend fun zapocniAnketu(idAnkete: Int, context: Context = MainContext.mainContext) : AnketaTaken? {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val poceteAnkete = ApiAdapter.retrofit.getPoceteAnkete().body()
                if (!poceteAnkete!!.map { it.AnketumId }.contains(idAnkete)) {
                    val response = ApiAdapter.retrofit.zapocniAnketu(idAnkete)
                    val responseBody = response.body()
                    if (responseBody != null && responseBody.student!=null) {
                        println("Response bodi nije null i on je " + responseBody.student)
                        db.anketaTakenDao().insertAnketaTaken(responseBody)
                    }

                    return@withContext responseBody
                }
            }

            val result = db.anketaTakenDao().getAnketaTaken()
            if(result.map { it.AnketumId }.contains(idAnkete))
                return@withContext result.filter { it.AnketumId == idAnkete }.first()

            return@withContext null
        }
    }

    suspend fun getPoceteAnkete(context: Context = MainContext.mainContext) : List<AnketaTaken>? {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val response = ApiAdapter.retrofit.getPoceteAnkete()
                val responseBody = response.body()
                if (responseBody?.isEmpty() == true)
                    return@withContext null

                return@withContext responseBody
            }

            return@withContext db.anketaTakenDao().getAnketaTaken()
        }
    }
}