package ba.etf.rma22.projekat.viewmodel

import android.content.Context
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.data.repositories.TakeAnketaRepository
import kotlinx.coroutines.*
import java.util.*

class AnketaListViewModel {
    val scope = MainScope()

    fun getMyAnkete(onSuccess: (ankete: List<Anketa>) -> Unit, onError: () -> Unit) {
        scope.launch {
            val ankete = AnketaRepository.getUpisane()
            when(ankete) {
                is List<Anketa> -> onSuccess.invoke(ankete)
                else -> onError.invoke()
            }
        }
    }

    fun getAllAnkete(onSuccess: (ankete: List<Anketa>) -> Unit, onError: () -> Unit) {
        scope.launch {
            val sveAnkete = AnketaRepository.getAll()

            when(sveAnkete) {
                is List<Anketa> -> onSuccess.invoke(sveAnkete)
                else -> onError.invoke()
            }
        }
    }


    fun getNotTaken(onSuccess: (ankete: List<Anketa>) -> Unit, onError: () -> Unit) {
        scope.launch {
            val sveAnkete = AnketaRepository.getAll()

            val notTaken = sveAnkete.filter { it.datumKraj!=null && it.datumKraj < Date() }.toList()

            when(notTaken) {
                is List<Anketa> -> onSuccess.invoke(notTaken)
                else -> onError.invoke()
            }
        }
    }

    fun getDone(onSuccess: (ankete: List<Anketa>) -> Unit, onError: () -> Unit) {
        scope.launch {
            val sveAnkete = AnketaRepository.getAll()
            val uzeteAnketaTaken = TakeAnketaRepository.getPoceteAnkete()

            val notTaken = arrayListOf<Anketa>()

            for(anketa in sveAnkete) {
                if (uzeteAnketaTaken != null) {
                    for(taken in uzeteAnketaTaken) {
                        if(taken.AnketumId == anketa.id && taken.progres.equals(100F))
                            notTaken.add(anketa)
                    }
                }
            }

            when(notTaken) {
                is List<Anketa> -> onSuccess.invoke(notTaken)
                else -> onError.invoke()
            }
        }
    }

    fun getFuture(onSuccess: (ankete: List<Anketa>) -> Unit, onError: () -> Unit) {
        scope.launch {
            val sveAnkete = AnketaRepository.getAll()
            val futureAnkete = sveAnkete.filter { it.datumPocetak > Date() }

            when(futureAnkete) {
                is List<Anketa> -> onSuccess.invoke(futureAnkete)
                else -> onError.invoke()
            }
        }
    }
}