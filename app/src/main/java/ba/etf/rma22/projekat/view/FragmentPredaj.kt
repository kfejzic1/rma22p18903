package ba.etf.rma22.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.repositories.OdgovorRepository

class FragmentPredaj : Fragment() {
    private lateinit var progresText : TextView
    private lateinit var btnPredaj : Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_predaj, container, false)

        progresText = view.findViewById(R.id.progresText)
        btnPredaj = view.findViewById(R.id.dugmePredaj)
        progresText.text = "0%"

//        if(anketa.datumKraj < Date() || anketa.datumRada!=null)
//            btnPredaj.isEnabled = false

        btnPredaj.setOnClickListener{
            OdgovorRepository.progres = 0F
            val act = (activity as MainActivity)
            act.removeAllFragments()
            act.addFragmentOnPosition(FragmentAnkete.newInstance(), 0)
            act.addFragmentOnPosition(FragmentPoruka.newInstance("Završili ste anketu!"), 1)
            act.setCurrentFragment(1, false)
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        progresText.text = (OdgovorRepository.progres*100).toString() + "%"
    }

    companion object {
        fun newInstance() : FragmentPredaj = FragmentPredaj().apply {
            //this.anketa = anketa
        }
    }


}