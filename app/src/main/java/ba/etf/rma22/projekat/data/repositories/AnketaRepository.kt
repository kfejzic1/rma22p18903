package ba.etf.rma22.projekat.data.repositories

import android.content.Context
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.api.ApiAdapter
import ba.etf.rma22.projekat.data.models.Anketa
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object AnketaRepository {
    suspend fun getAll(offset: Int = 0, context: Context = MainContext.mainContext) : ArrayList<Anketa> {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)
            if(ConnectivityRepository.connected) {
                val sveAnkete = arrayListOf<Anketa>()
                if (offset == 0) {
                    var i = 1
                    while (true) {
                        val ankete = ApiAdapter.retrofit.getAllAnkete(i).body()!!
                        sveAnkete.addAll(ankete)
                        i++

                        if (ankete.size != 5)
                            return@withContext sveAnkete
                    }
                }
                val response = ApiAdapter.retrofit.getAllAnkete(offset)
                val responseBody = response.body()

                if(db.anketaDao().getAll().isEmpty())
                    db.anketaDao().insertAnkete(responseBody!!)

                return@withContext responseBody!!
            }

            return@withContext db.anketaDao().getAll() as ArrayList<Anketa>
        }
    }

    suspend fun getById(id: Int, context: Context = MainContext.mainContext) : Anketa? {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val response = ApiAdapter.retrofit.getAnketaById(id)
                val responseBody = response.body()

                return@withContext responseBody
            }

            return@withContext db.anketaDao().getById(id)
        }
    }

    suspend fun getUpisane(context: Context = MainContext.mainContext) : List<Anketa> {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val upisaneGrupe = ApiAdapter.retrofit.getUpisaneGrupe().body()
                val upisaneAnkete = ArrayList<Anketa>()

                if (upisaneGrupe != null) {
                    for (grupa in upisaneGrupe) {
                        ApiAdapter.retrofit.getAnketeFromGrupa(grupa.id).body()
                            ?.let { upisaneAnkete.addAll(it) }
                    }
                }

                val sveAnkete = arrayListOf<Anketa>()

                var i = 1
                while (true) {
                    val ankete = ApiAdapter.retrofit.getAllAnkete(i).body()!!
                    sveAnkete.addAll(ankete)
                    i++

                    if (ankete.size != 5)
                        break
                }

                //db.anketaDao().deleteAllAnkete()
                db.anketaDao().insertAnkete(sveAnkete)
                db.anketaDao().updateAnketaIsTaken(upisaneAnkete.map{it.id})
                return@withContext upisaneAnkete
            }

            return@withContext db.anketaDao().getUpisane()
        }
    }
}