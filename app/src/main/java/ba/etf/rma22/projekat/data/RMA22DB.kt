package ba.etf.rma22.projekat.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ba.etf.rma22.projekat.data.dao.*
import ba.etf.rma22.projekat.data.models.*

@Database(entities =
[Anketa::class, AnketaiGrupe::class, AnketaTaken::class,
    Grupa::class, GrupaByAnketa::class, Istrazivanje::class,
    Odgovor::class, OdgovorPost::class, Pitanje::class,
    PitanjeAnketa::class, PostResponse::class, Account::class], version = 1)
@TypeConverters(DateConverter::class, ListStringConverter::class)
abstract class RMA22DB : RoomDatabase() {
    abstract fun anketaDao(): AnketaDao
    abstract fun accountDao(): AccountDao
    abstract fun istrazivanjeIGrupaDao() : IstrazivanjeIGrupaDao
    abstract fun anketaTakenDao() : AnketaTakenDao
    abstract fun odgovoriDao() : OdgovoriDao
    abstract fun pitanjeAnketaDao() : PitanjeAnketaDao
    companion object {
        private var INSTANCE: RMA22DB? = null
        fun getInstance(context: Context): RMA22DB {
            if(INSTANCE == null) {
                synchronized(RMA22DB::class) {
                    INSTANCE = buildRoomDB(context)
                }
            }
            return INSTANCE!!
        }
        private fun buildRoomDB(context: Context) =
            Room.databaseBuilder(context.applicationContext, RMA22DB::class.java, "RMA22DB").build()
    }
}