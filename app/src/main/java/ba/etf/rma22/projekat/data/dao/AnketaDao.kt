package ba.etf.rma22.projekat.data.dao

import androidx.room.*
import ba.etf.rma22.projekat.data.models.Anketa

@Dao
interface AnketaDao {
    @Query("SELECT * FROM anketa")
    fun getAll(): List<Anketa>

    @Query("SELECT * FROM anketa WHERE isTaken=1")
    fun getUpisane(): List<Anketa>

    @Query("SELECT * FROM anketa WHERE id=:id")
    fun getById(id : Int) : Anketa

    @Query("SELECT a.* FROM anketa a JOIN anketaTaken at ON a.id=at.AnketumId WHERE at.id=:id")
    fun getAnketaByAnketaTaken(id: Int) : List<Anketa>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAnkete(ankete: List<Anketa>)

    @Query("UPDATE anketa SET isTaken=1 WHERE id IN (:anketeId)")
    fun updateAnketaIsTaken(anketeId : List<Int>)

    @Query("DELETE FROM anketa")
    fun deleteAllAnkete()
}