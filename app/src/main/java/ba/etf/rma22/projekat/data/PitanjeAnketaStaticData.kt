package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.models.PitanjeAnketa
//import ba.etf.rma22.projekat.data.repositories.AnketaRepository.korisnik
//import ba.etf.rma22.projekat.data.repositories.AnketaRepository.sveAnkete

fun getAllPitanjeAnketa() : List<PitanjeAnketa> {
    return listOf(
//        PitanjeAnketa(korisnik.ankete[0], getAllPitanje()[0].naziv, null),
//        PitanjeAnketa(korisnik.ankete[0], getAllPitanje()[1].naziv, null),
//        PitanjeAnketa(korisnik.ankete[0], getAllPitanje()[2].naziv, null),
//        PitanjeAnketa(korisnik.ankete[0], getAllPitanje()[3].naziv, null),
//        PitanjeAnketa(sveAnkete[7], getAllPitanje()[4].naziv, null),
//        PitanjeAnketa(sveAnkete[7], getAllPitanje()[5].naziv, null),
//        PitanjeAnketa(sveAnkete[7], getAllPitanje()[6].naziv, null),
//        PitanjeAnketa(sveAnkete[17], getAllPitanje()[7].naziv, 1),
//        PitanjeAnketa(sveAnkete[17], getAllPitanje()[8].naziv, 1)
        )
}

fun getAllPitanje() : List<Pitanje> {
    return listOf(
//        Pitanje("Prvo pitanje", "Da li ste ikada koristili ovu aplikaciju?", listOf("Jesam", "Nisam")),
//        Pitanje("Drugo pitanje", "Kako ste čuli za ovu aplikaciju?", listOf("Televizija", "Novine", "Radio", "Internet", "Ostalo")),
//        Pitanje("Treće pitanje", "Koliko mislite da je ova aplikacija inovativna?", listOf("1", "2", "3", "4", "5")),
//        Pitanje("Četvrto pitanje", "Mislite li da je aplikacija korisna?", listOf("Da", "Ne")),
//        Pitanje("Peto pitanje", "U kojem vremenskom periodu najčešće koristite ovaj proizvod?", listOf("Jutro", "Prijepodne", "Poslijepodne", "Navečer")),
//        Pitanje("Šesto pitanje", "Da li biste drugima predložili ovaj proizvod?", listOf("Da", "Ne")),
//        Pitanje("Sedmo pitanje", "Kako bi ocijenio iskustvo sa ovim proizvodom?", listOf("Odlično", "Vrlodobro", "Dobro", "Zadovoljavajuće", "Nedovoljno sadržaja")),
//        Pitanje("Osmo pitanje", "Koliko je vjerovatno da proizvod predložite poznanicima?", listOf("0", "1", "2", "3", "4", "5")),
//        Pitanje("Deveto pitanje", "Ocijenite dizajn aplikacije!", listOf("1", "2", "3", "4", "5")),
//        Pitanje("Deseto pitanje", "Koji od navedenih opisuje vaš radni status?", listOf("Nezaposlen", "Student", "Zaposlen", "Poslodavac")),
        )
}