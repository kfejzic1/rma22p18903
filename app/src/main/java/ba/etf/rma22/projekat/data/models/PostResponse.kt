package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class PostResponse(
    @PrimaryKey val id: Int,
    @ColumnInfo(name="message") @SerializedName("message") val poruka : String,
)
