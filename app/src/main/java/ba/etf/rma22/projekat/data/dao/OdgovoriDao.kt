package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.models.OdgovorPost

@Dao
interface OdgovoriDao {
    @Query("SELECT * FROM odgovor WHERE AnketaTakenId=:anketaTakenId")
    suspend fun getOdgovoriAnketa(anketaTakenId : Int) : List<Odgovor>

    @Query("SELECT * FROM odgovor")
    suspend fun getOdgovori() : List<Odgovor>

    @Insert
    suspend fun insertOdgovor(odgovor: Odgovor)

    @Delete
    suspend fun deleteOdgovori(odgovor: List<Odgovor>)
}