package ba.etf.rma22.projekat.data.repositories

import android.content.Context
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.models.Account
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object AccountRepository {
    var acHash : String = "98220176-e34b-42db-bc6e-026d2135d41f"

    suspend fun postaviHash(acHash : String, context: Context = MainContext.mainContext) : Boolean {
        this.acHash = acHash
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)
            db.accountDao().deleteAccount()
            db.accountDao().insertAccount(Account(1, acHash))
            return@withContext true
        }
    }

    fun getHash() : String {
        return acHash
    }
}