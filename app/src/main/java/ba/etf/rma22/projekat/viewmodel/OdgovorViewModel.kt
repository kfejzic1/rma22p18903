package ba.etf.rma22.projekat.viewmodel

import android.content.Context
import ba.etf.rma22.projekat.data.repositories.OdgovorRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class OdgovorViewModel {
    val scope = GlobalScope

    fun postaviOdgovorAnketa(idAnketaTaken: Int, idPitanja: Int, odgovor: Int, onSuccess: () -> Unit, onError: () -> Unit)  {
        scope.launch {
            val ankete = OdgovorRepository.postaviOdgovorAnketa(idAnketaTaken, idPitanja, odgovor)
            when(ankete) {
                is Int -> onSuccess.invoke()
                else -> onError.invoke()
            }
        }
    }

    fun updateProgres(progres : Float)  {
        OdgovorRepository.progres = progres
    }

    fun getProgres() : Float {
        return OdgovorRepository.progres
    }
}