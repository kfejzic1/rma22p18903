package ba.etf.rma22.projekat.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R


class FragmentPoruka : Fragment() {
    private lateinit var output : TextView
    private lateinit var poruka : String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_poruka, container, false)

        output = view.findViewById(R.id.tvPoruka)
        output.text = poruka

        return view
    }

    override fun onPause() {
        super.onPause()
        val act = (activity as MainActivity)

        //Obezbjeđuje da se izvrše sve prethodne transakcije, pa tek onda da izvrši update fragmenta
        val uiHandler = Handler(Looper.getMainLooper())
        uiHandler.post { act.updateFragmentOnPosition(FragmentIstrazivanje.newInstance(), 1) }
    }

    companion object {
        fun newInstance(poruka : String) : FragmentPoruka = FragmentPoruka().apply {
            this.poruka = poruka
        }
    }
}