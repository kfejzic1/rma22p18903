package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.Istrazivanje

fun dajAll() : List<Istrazivanje> {
    return listOf(
//        Istrazivanje("Prvo istraživanje", 2),
//        Istrazivanje("Drugo istraživanje", 3),
//        Istrazivanje("Treće istraživanje", 1),
//        Istrazivanje("Četvrto istraživanje", 4),
//        Istrazivanje("Peto istraživanje", 5),
//        Istrazivanje("Šesto istraživanje", 1),
//        Istrazivanje("Sedmo istraživanje", 3),
//        Istrazivanje("Osmo istraživanje", 3)
    )
}

fun dajUpisani() : List<Istrazivanje> {
    return dajAll().filter { istrazivanje -> istrazivanje.naziv == "Prvo istraživanje" || istrazivanje.naziv == "Drugo istraživanje"}
}