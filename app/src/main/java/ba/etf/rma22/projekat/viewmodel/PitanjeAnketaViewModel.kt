package ba.etf.rma22.projekat.viewmodel

import android.content.Context
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class PitanjeAnketaViewModel {
    val scope = MainScope()

    fun getPitanjaOfAnketa(idAnkete: Int, onSuccess: (pitanja: List<Pitanje>) -> Unit, onError: () -> Unit)  {
        scope.launch {
            val ankete = PitanjeAnketaRepository.getPitanja(idAnkete)
            when(ankete) {
                is List<Pitanje> -> onSuccess.invoke(ankete)
                else -> onError.invoke()
            }
        }
    }
}