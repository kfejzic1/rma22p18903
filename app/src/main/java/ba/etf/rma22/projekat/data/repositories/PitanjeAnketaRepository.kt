package ba.etf.rma22.projekat.data.repositories

import android.content.Context
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.api.ApiAdapter
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.models.PitanjeAnketa
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object PitanjeAnketaRepository {
    suspend fun getPitanja(idAnkete : Int, context: Context = MainContext.mainContext) : List<Pitanje> {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val response = ApiAdapter.retrofit.getPitanja(idAnkete)
                val responseBody = response.body()

                if(!db.pitanjeAnketaDao().getAllPitanjeAnketa().map { it.id }.contains(idAnkete)){
                    //Pitanja nisu upisana u bazu
                    db.pitanjeAnketaDao().insertPitanja(responseBody!!)
                    var noviId = db.pitanjeAnketaDao().getPitanjaAnketa().size+1
                    responseBody.forEach { db.pitanjeAnketaDao().insertPitanjeAnketa(PitanjeAnketa(noviId++, idAnkete, it.naziv)) }
                }

                return@withContext responseBody!!
            }

            return@withContext db.pitanjeAnketaDao().getPitanja(idAnkete)
        }
    }
}