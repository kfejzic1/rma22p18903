package ba.etf.rma22.projekat.view

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import ba.etf.rma22.projekat.data.models.Grupa


class GrupaSpinnerAdapter(
    contextC: Context, textViewResourceId: Int,
    values: Array<Grupa>
) : ArrayAdapter<Grupa>(contextC, textViewResourceId, values) {
    // Your sent context
    private val privateContext: Context

    // Your custom values for the spinner (User)
    private val values: Array<Grupa>
    override fun getCount(): Int {
        return values.size
    }

    override fun getItem(position: Int): Grupa {
        return values[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        val label = super.getView(position, convertView, parent) as TextView
        label.setTextColor(Color.WHITE)
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.text = values[position].naziv

        // And finally return your dynamic (or custom) view for each spinner item
        return label
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    override fun getDropDownView(
        position: Int, convertView: View?,
        parent: ViewGroup?
    ): View {
        val label = super.getDropDownView(position, convertView, parent!!) as TextView
        label.setTextColor(Color.WHITE)
        label.text = values[position].naziv
        return label
    }

    init {
        this.privateContext = contextC
        this.values = values
    }
}