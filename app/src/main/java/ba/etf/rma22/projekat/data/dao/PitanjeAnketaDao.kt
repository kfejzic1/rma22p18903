package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.models.PitanjeAnketa

@Dao
interface PitanjeAnketaDao {
    @Query("SELECT * FROM pitanje p JOIN pitanjeAnketa pa ON p.naziv=pa.PitanjeId WHERE pa.AnketumId=:anketaId")
    suspend fun getPitanja(anketaId : Int) : List<Pitanje>

    @Query("SELECT * FROM pitanje")
    suspend fun getAllPitanja() : List<Pitanje>

    @Query("SELECT * FROM pitanjeanketa")
    suspend fun getAllPitanjeAnketa() : List<PitanjeAnketa>

    @Query("SELECT * FROM pitanjeAnketa")
    suspend fun getPitanjaAnketa() : List<PitanjeAnketa>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPitanja(pitanja :List<Pitanje>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertPitanjeAnketa(pitanjeAnketa: PitanjeAnketa)
}