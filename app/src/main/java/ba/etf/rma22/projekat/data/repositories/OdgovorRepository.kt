package ba.etf.rma22.projekat.data.repositories

import android.content.Context
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.api.ApiAdapter
import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.models.OdgovorPost
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object OdgovorRepository {
    var progres : Float = 0.0f

    suspend fun getOdgovoriAnketa(idAnkete : Int, context: Context = MainContext.mainContext) : List<Odgovor> {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val poceteAnkete = ApiAdapter.retrofit.getPoceteAnkete().body()
                val anketaTakenId = poceteAnkete!!.filter { it.AnketumId == idAnkete }.map { it.id }.first()

                val response = ApiAdapter.retrofit.getOdgovoriAnketa(anketaTakenId)
                val responseBody = response.body() ?: return@withContext listOf()

                return@withContext responseBody
            }

            val poceteAnkete = db.anketaTakenDao().getAnketaTaken()
            val anketaTakenId = poceteAnkete.filter { it.AnketumId == idAnkete }.map { it.id }.first()

            return@withContext db.odgovoriDao().getOdgovoriAnketa(anketaTakenId)
        }
    }

    suspend fun postaviOdgovorAnketa(idAnketaTaken: Int, idPitanja: Int, odgovor: Int, context: Context = MainContext.mainContext) : Int {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            val poceteAnkete = ApiAdapter.retrofit.getPoceteAnkete().body()

            val idAnkete = poceteAnkete!!.filter { it.id == idAnketaTaken }.map{it.AnketumId}.first()
            val brojPitanjaAnkete = ApiAdapter.retrofit.getPitanja(idAnkete).body()!!.size.toFloat()

            val odgovorenaPitanja = ApiAdapter.retrofit.getOdgovoriAnketa(idAnketaTaken).body()
            var brojOdgovorenihPitanja = 0
            if(odgovorenaPitanja != null)
                brojOdgovorenihPitanja = odgovorenaPitanja.size

            val noviProgres = StaticFunctions.updateProgressWithStep2((brojOdgovorenihPitanja+1)/brojPitanjaAnkete)
            ApiAdapter.retrofit.postaviOdgovorAnketaTaken(idAnketaTaken, OdgovorPost(odgovor, idPitanja, noviProgres.toFloat()))

            //Insert odgovor
            db.odgovoriDao().deleteOdgovori(db.odgovoriDao().getOdgovori())
            db.odgovoriDao().insertOdgovor(Odgovor(1, odgovor, idPitanja, idAnketaTaken))
            db.anketaTakenDao().updateAnketaTakenProgres(idAnketaTaken, noviProgres.toFloat())

            return@withContext noviProgres
        }
    }
}