package ba.etf.rma22.projekat.data.repositories

import android.content.Context
import ba.etf.rma22.projekat.data.RMA22DB
import ba.etf.rma22.projekat.data.api.ApiAdapter
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.GrupaByAnketa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object IstrazivanjeIGrupaRepository {
    suspend fun getIstrazivanja(offset: Int = 0, context: Context = MainContext.mainContext) : List<Istrazivanje> {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                if (offset == 0) {
                    var i = 1
                    val result = arrayListOf<Istrazivanje>()
                    while (true) {
                        val response = ApiAdapter.retrofit.getIstrazivanja(i)
                        val responseBody = response.body()
                        result.addAll(responseBody!!)
                        if (responseBody.size != 5) {
                            db.istrazivanjeIGrupaDao().insertIstrazivanja(result)
                            return@withContext result
                        }
                        i++
                    }
                } else {
                    val response = ApiAdapter.retrofit.getIstrazivanja(offset)
                    val responseBody = response.body()
                    db.istrazivanjeIGrupaDao().deleteIstrazivanja(db.istrazivanjeIGrupaDao().getIstrazivanja())
                    db.istrazivanjeIGrupaDao().insertIstrazivanja(responseBody!!)
                    return@withContext responseBody
                }
            }
            return@withContext db.istrazivanjeIGrupaDao().getIstrazivanja()
        }
    }

    suspend fun getGrupe(context: Context = MainContext.mainContext) : List<Grupa> {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val response = ApiAdapter.retrofit.getGrupe()
                val responseBody = response.body()
                db.istrazivanjeIGrupaDao().insertGrupe(responseBody!!)
                return@withContext responseBody
            }
            return@withContext db.istrazivanjeIGrupaDao().getGrupe()
        }
    }

    suspend fun getGrupeZaIstrazivanje(idIstrazivanja: Int, context: Context = MainContext.mainContext) : List<Grupa> {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val sveGrupe = ApiAdapter.retrofit.getGrupe().body()
                val filtriraneGrupe = sveGrupe?.filter { it.istrazivanjeId == idIstrazivanja }

                return@withContext filtriraneGrupe!!
            }
            return@withContext db.istrazivanjeIGrupaDao().getGrupeZaIstrazivanja(idIstrazivanja)
        }
    }

    suspend fun upisiUGrupu(idGrupa: Int, context: Context = MainContext.mainContext) : Boolean {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            val response = ApiAdapter.retrofit.upisiUGrupu(idGrupa)
            val responseBody = response.body()

            if(db.istrazivanjeIGrupaDao().getGrupe().isEmpty())
                db.istrazivanjeIGrupaDao().insertGrupe(ApiAdapter.retrofit.getGrupe().body()!!)

            db.istrazivanjeIGrupaDao().updateIsUpisaneGrupe(idGrupa)

            if(responseBody != null && responseBody.poruka.contains("je dodan u grupu")) {
                return@withContext true
            }

            return@withContext false
        }
    }

    suspend fun getUpisaneGrupe(context: Context = MainContext.mainContext) : List<Grupa> {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val response = ApiAdapter.retrofit.getUpisaneGrupe()
                val responseBody = response.body()
                return@withContext responseBody!!
            }

            return@withContext db.istrazivanjeIGrupaDao().getUpisaneGrupe()
        }
    }

    suspend fun getGrupeByAnketa(idAnkete: Int, context: Context = MainContext.mainContext) : List<GrupaByAnketa> {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val response = ApiAdapter.retrofit.getGrupeByAnketa(idAnkete)
                val responseBody = response.body()

                db.istrazivanjeIGrupaDao().insertGrupeByAnketa(responseBody!!)

                return@withContext responseBody
            }

            return@withContext db.istrazivanjeIGrupaDao().getGrupeByAnketa(idAnkete)
        }
    }

    suspend fun getIstrazivanjeById(idIstrazivanja: Int, context: Context = MainContext.mainContext) : Istrazivanje {
        return withContext(Dispatchers.IO) {
            val db = RMA22DB.getInstance(context)

            if(ConnectivityRepository.connected) {
                val response = ApiAdapter.retrofit.getIstrazivanjaById(idIstrazivanja)
                val responseBody = response.body()
                //Mozda je potrebno dodati sva istrazivanja u bazu, ako se getIstrazivanjeById poziva prije getIstrazivanja
                return@withContext responseBody!!
            }

            return@withContext db.istrazivanjeIGrupaDao().getIstrazivanjeById(idIstrazivanja)
        }
    }
}