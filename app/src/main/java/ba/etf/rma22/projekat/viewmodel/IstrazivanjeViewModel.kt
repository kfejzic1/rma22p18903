package ba.etf.rma22.projekat.viewmodel

import android.content.Context
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class IstrazivanjeViewModel {
    val scope = MainScope()

    fun getNotTakenIstrazivanjeOnGodina(godina : Int, onSuccess: (istrazivanja: List<Istrazivanje>) -> Unit, onError: () -> Unit) {
        scope.launch {
            var svaIstrazivanja = ArrayList<Istrazivanje>()

            var i = 1
            while(true) {
                val page = IstrazivanjeIGrupaRepository.getIstrazivanja(i)
                svaIstrazivanja.addAll(page)

                if(page.size != 5)
                    break
                i++
            }

            val istrazivanjaNaGodini = svaIstrazivanja.filter { it.godina == godina }

            when(istrazivanjaNaGodini) {
                is List<Istrazivanje> -> onSuccess.invoke(istrazivanjaNaGodini)
                else -> onError.invoke()
            }
        }
    }
}