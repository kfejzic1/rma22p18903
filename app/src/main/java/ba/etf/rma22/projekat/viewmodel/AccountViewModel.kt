package ba.etf.rma22.projekat.viewmodel

import android.content.Context
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AccountRepository
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class AccountViewModel {
    val scope = MainScope()

    fun postaviHash(acHash : String, onSuccess: () -> Unit, onError: () -> Unit) {
        scope.launch {
            val success = AccountRepository.postaviHash(acHash)
            when(success) {
                is Boolean -> onSuccess.invoke()
                else -> onError.invoke()
            }
        }
    }
}