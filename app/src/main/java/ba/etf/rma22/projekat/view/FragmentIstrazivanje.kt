package ba.etf.rma22.projekat.view

//import ba.etf.rma22.projekat.data.repositories.AnketaRepository.korisnik
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.ConnectivityRepository
import ba.etf.rma22.projekat.viewmodel.GrupaViewModel
import ba.etf.rma22.projekat.viewmodel.IstrazivanjeViewModel

class FragmentIstrazivanje : Fragment() {
    private lateinit var odabirGodina : Spinner
    private lateinit var odabirIstrazivanja : Spinner
    private lateinit var odabirGrupa : Spinner
    private lateinit var potvrdi : Button
    private val istrazivanjeViewModel = IstrazivanjeViewModel()
    private val grupeViewModel = GrupaViewModel()
    private var upisaneGrupe = listOf<Grupa>()
    private lateinit var odabranaGrupa : Grupa
    private lateinit var odabranoIstrazivanje : Istrazivanje

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_istrazivanje, container, false)

        odabirGodina = view.findViewById(R.id.odabirGodina)
        odabirIstrazivanja = view.findViewById(R.id.odabirIstrazivanja)
        odabirGrupa = view.findViewById(R.id.odabirGrupa)

        odabirGodina.isEnabled = ConnectivityRepository.connected
        odabirIstrazivanja.isEnabled = ConnectivityRepository.connected
        odabirGrupa.isEnabled = ConnectivityRepository.connected

        potvrdi = view.findViewById(R.id.dodajIstrazivanjeDugme)
        potvrdi.isEnabled = false


        if(ConnectivityRepository.connected) {
            val adapter = context?.let {
                ArrayAdapter(
                    it,
                    R.layout.spinner_item,
                    arrayOf("1", "2", "3", "4", "5")
                )
            }
            adapter?.setDropDownViewResource(R.layout.spinner_dropdown_item)
            odabirGodina.adapter = adapter

            odabirGodina.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                        istrazivanjeViewModel.getNotTakenIstrazivanjeOnGodina(
                            odabirGodina.selectedItem.toString().toInt(),
                            onSuccess = ::onSuccessNotTakenIstrazivanja,
                            onError = ::onError
                        )
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {
                    //Uvijek je nešto selectovano
                }
            }

            potvrdi.setOnClickListener {
                val selectedGrupa = odabirGrupa.selectedItem as Grupa
                    grupeViewModel.upisiUGrupu(
                        selectedGrupa.id,
                        onSuccess = ::onSuccessUpisGrupa,
                        onError = ::onError
                    )
            }
        }
        else {
            potvrdi.text = "Onemogućeno"
            potvrdi.isEnabled = false
        }

        return view
    }

    private fun onSuccessUpisGrupa() {
        (activity as MainActivity).updateFragmentOnPosition(FragmentPoruka.newInstance(
            "Uspješno ste upisani u grupu ${odabranaGrupa.naziv} istraživanja ${odabranoIstrazivanje.naziv}!"
        ), 1)
    }

    private fun onSuccessNotTakenIstrazivanja(istrazivanja: List<Istrazivanje>) {
        updateIstrazivanjeSpinner(istrazivanja.toTypedArray())

        grupeViewModel.getUpisaneGrupe(onSuccess = ::onSuccessUpisaneGrupe, onError = ::onError)
    }

    private fun onSuccessUpisaneGrupe(upisaneGrupe : List<Grupa>) {
        this.upisaneGrupe = upisaneGrupe

        odabirIstrazivanja.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedItem = odabirIstrazivanja.selectedItem as Istrazivanje
                odabranoIstrazivanje = selectedItem
                grupeViewModel.getGrupeZaIstrazivanje(selectedItem.id, onSuccess = ::onSuccessGrupeZaIstrazivanja, onError = ::onError)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                //Nothing
            }
        }
    }

    private fun onSuccessGrupeZaIstrazivanja(grupe: List<Grupa>) {
        val notUpisaneGrupe = grupe.filterNot { upisaneGrupe.contains(it) }
        updateGrupeSpinner(notUpisaneGrupe.toTypedArray())

        odabirGrupa.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedItem = odabirGrupa.selectedItem as Grupa
                odabranaGrupa = selectedItem
                potvrdi.isEnabled = true
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                //Mora biti selektovano
            }
        }
    }

    companion object {
        fun newInstance() : FragmentIstrazivanje = FragmentIstrazivanje()
    }

    private fun updateGrupeSpinner(vrijednosti : Array<Grupa>) {
        val adapter = context?.let { GrupaSpinnerAdapter(it, R.layout.spinner_item, vrijednosti) }
        adapter?.setDropDownViewResource(R.layout.spinner_dropdown_item)
        odabirGrupa.adapter = adapter
    }

    private fun updateIstrazivanjeSpinner(vrijednosti: Array<Istrazivanje>) {
        val adapter = context?.let { IstrazivanjeSpinnerAdapter(it, R.layout.spinner_item, vrijednosti) }
        adapter?.setDropDownViewResource(R.layout.spinner_dropdown_item)
        odabirIstrazivanja.adapter = adapter
    }

//    private fun aktivirajDugmePotvrdi() {
//        potvrdi.isEnabled = odabirIstrazivanja.selectedItem!=null && odabirGrupa.selectedItem!=null
//    }

    fun onError() {
        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }
}