package ba.etf.rma22.projekat.data.dao

import androidx.room.*
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.GrupaByAnketa
import ba.etf.rma22.projekat.data.models.Istrazivanje

@Dao
interface IstrazivanjeIGrupaDao {
    @Query("SELECT * FROM grupaByAnketa WHERE id=:id")
    fun getGrupeByAnketa(id: Int) : List<GrupaByAnketa>

    @Query("SELECT * FROM grupa")
    fun getGrupe() : List<Grupa>

    @Query("SELECT * FROM istrazivanje WHERE id=:id")
    fun getIstrazivanjeById(id: Int) : Istrazivanje

    @Query("SELECT * FROM istrazivanje")
    fun getIstrazivanja() : List<Istrazivanje>

    @Query("SELECT * FROM grupa WHERE IstrazivanjeId=:idIstrazivanja")
    fun getGrupeZaIstrazivanja(idIstrazivanja: Int) : List<Grupa>

    @Query("SELECT * FROM grupa WHERE isUpisan=1")
    fun getUpisaneGrupe() : List<Grupa>

    @Query("UPDATE grupa SET isUpisan=1 WHERE id=:id")
    fun updateIsUpisaneGrupe(id : Int)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertIstrazivanja(istrazivanje: List<Istrazivanje>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertGrupe(grupe : List<Grupa>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertGrupeByAnketa(grupeByAnketa : List<GrupaByAnketa>)

    @Delete
    fun deleteIstrazivanja(istrazivanje: List<Istrazivanje>)
}