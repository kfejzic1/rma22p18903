package ba.etf.rma22.projekat.data.repositories

import java.util.*
import kotlin.math.roundToInt

object StaticFunctions {
    fun updateProgressWithStep2(progress : Float) : Int {
        var newValue = (progress * 10).roundToInt()
        //Definišemo step od 0.2
        if(newValue % 2 != 0)
            newValue+=1
        return newValue*10
    }

    fun umanjiDatumZaJedanMjesec(datum : Date?) : Date {
        val cal = Calendar.getInstance()
        cal.time = datum
        cal.add(Calendar.MONDAY, -1)
        return cal.time
    }
}