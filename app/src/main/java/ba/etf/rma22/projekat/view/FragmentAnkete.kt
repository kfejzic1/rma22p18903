package ba.etf.rma22.projekat.view

//import ba.etf.rma22.projekat.data.repositories.AnketaRepository.korisnik
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.viewmodel.AnketaListViewModel
import ba.etf.rma22.projekat.viewmodel.AnketaTakenViewModel
import ba.etf.rma22.projekat.viewmodel.GrupaViewModel
import ba.etf.rma22.projekat.viewmodel.PitanjeAnketaViewModel
import java.util.*


class FragmentAnkete : Fragment() {
    private lateinit var ankete : RecyclerView
    private lateinit var anketeAdapter : AnketeListAdapter
    private lateinit var spinner : Spinner
    private var anketeListViewModel = AnketaListViewModel()
    private var pitanjeAnketaViewModel = PitanjeAnketaViewModel()
    private var anketaTakenViewModel = AnketaTakenViewModel()
    private var grupeViewModel = GrupaViewModel()
    private var refAnkete = listOf<Anketa>()
    private lateinit var anketaTaken : AnketaTaken

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) :View? {
        val view = inflater.inflate(R.layout.fragment_ankete, container, false)

        anketaTakenViewModel.getPoceteAnkete(onSuccess = ::onSuccessPoceteAnkete, onError = ::onError)

        ankete = view.findViewById(R.id.listaAnketa)
        ankete.layoutManager = GridLayoutManager(view.context, 2)

        anketeAdapter = AnketeListAdapter(listOf()) {anketa ->  showFragmentPitanje(anketa)}
        ankete.adapter = anketeAdapter

        spinner = view.findViewById(R.id.filterAnketa)
        val adapter = ArrayAdapter(view.context, R.layout.spinner_item, listOf("Sve moje ankete", "Sve ankete", "Buduće ankete", "Prošle ankete", "Urađene ankete"))
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(parent?.getItemAtPosition(position)) {
                    "Sve ankete" -> anketeListViewModel.getAllAnkete(onSuccess = ::onSuccessAllAnkete, onError = ::onError)
                    "Buduće ankete" -> anketeListViewModel.getFuture(onSuccess = ::onSuccessAllAnkete, onError = ::onError)
                    "Prošle ankete" -> anketeListViewModel.getNotTaken(onSuccess = ::onSuccessAllAnkete, onError = ::onError)
                    "Urađene ankete" -> anketeListViewModel.getDone(onSuccess = ::onSuccessAllAnkete, onError = ::onError)
                    else -> anketeListViewModel.getMyAnkete(onSuccess = ::onSuccessAllAnkete, onError = ::onError)
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                //Uvijek je nešto selectovano
                println("Ovdje prekida")
            }
        }

        return view
    }

    private fun onSuccessPoceteAnkete(anketaTaken: List<AnketaTaken>) {
        println("Postavljam Ankete!")
        anketeAdapter.postaviAnketaTaken(anketaTaken)
    }

    fun showFragmentPitanje(anketa : Anketa) {
        if(anketa.datumPocetak > Date())
            return

        if(!refAnkete.map { x -> x.id }.contains(anketa.id)) {
            val toast = Toast.makeText(context, "Niste upisani na istraživanje sa ovom anketom!", Toast.LENGTH_SHORT)
            toast.show()
            return
        }

        anketaTakenViewModel.zapocniAnketu(anketa.id, onSuccess = ::onSuccessZapocniAnketu, onError = ::onError)
    }

    private fun onSuccessZapocniAnketu(anketaTaken : AnketaTaken) {
        this.anketaTaken = anketaTaken
        pitanjeAnketaViewModel.getPitanjaOfAnketa(anketaTaken.AnketumId, onSuccess = ::onSuccessPitanja, onError = ::onError)
    }

    fun onSuccessAllAnkete(ankete:List<Anketa>){
        dodijeliIstrazivanjaAnketama(ankete)
    }

    fun dodijeliIstrazivanjaAnketama(ankete: List<Anketa>) {
        this.refAnkete = ankete
        grupeViewModel.getGroupsByManyAnkete(ankete, onSuccess = ::onSuccessGroupsByManyAnkete, onError = ::onError)
    }

    private fun onSuccessGroupsByManyAnkete(mapa: MutableMap<Int, Istrazivanje>) {
        anketeAdapter.postaviMapuAnketaIdIstrazivanje(mapa)
        anketeAdapter.updateAnkete(refAnkete)
    }


    fun onSuccessPitanja(pitanja:List<Pitanje>) {
        if(pitanja.isEmpty()) {
            Toast.makeText(context, "Za ovu anketu nisu definisana pitanja", Toast.LENGTH_SHORT).show()
            return
        }

        (activity as MainActivity).removeAllFragments()

        for(i in 0..pitanja.size-1) {
            (activity as MainActivity).addFragmentOnPosition(
                FragmentPitanje.newInstance(pitanja[i], this.anketaTaken), i)
        }

        (activity as MainActivity).addFragmentOnPosition(FragmentPredaj.newInstance(), pitanja.size)
    }

    fun onError() {
        println("U OnErroru sam!")

        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }

    companion object {
        fun newInstance() : FragmentAnkete = FragmentAnkete()
    }
}
