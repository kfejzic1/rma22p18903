package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Account

@Dao
interface AccountDao {
    @Query("DELETE FROM account")
    suspend fun deleteAccount()

    @Insert
    suspend fun insertAccount(account: Account)
}