package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class PitanjeAnketa(
    @PrimaryKey val primaryKeyId: Int,
    @ColumnInfo(name="AnketumId") @SerializedName("AnketumId") val id : Int,
    @ColumnInfo(name="PitanjeId") @SerializedName("PitanjeId") val naziv : String,
)
