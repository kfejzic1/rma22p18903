package ba.etf.rma22.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken

@Dao
interface AnketaTakenDao {
    @Query("SELECT * FROM anketaTaken")
    fun getAnketaTaken() : List<AnketaTaken>

    @Query("UPDATE anketaTaken SET progres=:progres WHERE id=:id")
    fun updateAnketaTakenProgres(id: Int, progres: Float)

    @Insert
    fun insertAnketaTaken(anketaTaken: AnketaTaken)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAnketaTakens(anketaTakens: List<AnketaTaken>)
}