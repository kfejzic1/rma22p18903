package ba.etf.rma22.projekat.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Istrazivanje
import java.text.SimpleDateFormat
import java.util.*

class AnketeListAdapter (
    private var ankete : List<Anketa>,
    private val onItemClicked: (anketa : Anketa) -> Unit) :
    RecyclerView.Adapter<AnketeListAdapter.AnketeViewHolder>(){
    private var mapaAnketaIdIstrazivanje = mapOf<Int, Istrazivanje>()
    private var anketeTaken = listOf<AnketaTaken>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnketeListAdapter.AnketeViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_anketa, parent, false)
        return AnketeViewHolder(view)
    }

    override fun onBindViewHolder(holder: AnketeListAdapter.AnketeViewHolder, position: Int) {
        //Pronalazimo id drawable elementa na osnovu vracene vrijednosti anketaStatus funkcije
        val context : Context = holder.anketaStatus.context
        val statusMatch : String = anketaStatus(ankete[position])
        val id : Int = context.resources.getIdentifier(statusMatch, "drawable", context.packageName)
        if (id == 0)    //Nije identifikovan status, provjeriti funkciju anketaStatus
            println("Greška, provjerit funkciju anketaStatus")
        holder.anketaStatus.setImageResource(id)

        holder.anketaName.text = ankete[position].naziv

        var progressAnkete = 0.0f
        println("AnketeTakenSize: " + anketeTaken.size)
        if(anketeTaken.size != 0) {
            val lastAnketaTaken = anketeTaken.filter { it.AnketumId == ankete[position].id }.maxByOrNull { it.id }
            if(lastAnketaTaken != null) {
                progressAnkete = lastAnketaTaken.progres
            }
        }
        holder.anketaProgress.progress = progressAnkete.toInt()

        holder.anketaIstrazivanje.text = mapaAnketaIdIstrazivanje.get(ankete[position].id)?.naziv

        holder.itemView.setOnClickListener{onItemClicked(ankete[position])}
        holder.anketaDatum.text = anketaDate(statusMatch, ankete[position])
    }

    override fun getItemCount(): Int = ankete.size

    inner class AnketeViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val anketaName : TextView = itemView.findViewById(R.id.anketa_naziv)
        val anketaStatus : ImageView = itemView.findViewById(R.id.anketa_status)
        val anketaIstrazivanje : TextView = itemView.findViewById(R.id.anketa_nazivIstrazivanja)
        val anketaProgress : ProgressBar = itemView.findViewById(R.id.progresZavrsetka)
        val anketaDatum : TextView = itemView.findViewById(R.id.anketa_datum)
    }

    fun updateAnkete(ankete : List<Anketa>) {
        this.ankete = ankete.sortedBy { anketa -> anketa.datumPocetak.time }
        notifyDataSetChanged()
    }

    private fun anketaStatus(anketa: Anketa) : String {
        if(anketeTaken.map { x -> x.AnketumId }.contains(anketa.id))
            return "plava"
        else if(anketa.datumPocetak <= Date() && (anketa.datumKraj == null || anketa.datumKraj >= Date()))
            return "zelena"
        else if(anketa.datumPocetak > Date())
            return "zuta"

        return "crvena"
    }

    private fun anketaDate(color : String, anketa : Anketa) : String {
        val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy")

        var datumRadaAnkete = Date()
        val filtrirano = anketeTaken.filter { x -> x.AnketumId == anketa.id }
        if(filtrirano.isNotEmpty())
            datumRadaAnkete = filtrirano.map { x -> x.datumRada }.last()!!

        var datumKrajaAnkete: String
        if(anketa.datumKraj == null)
            datumKrajaAnkete = "nije određeno"
        else
            datumKrajaAnkete = simpleDateFormat.format(anketa.datumKraj)

        if(color == "plava")
            return "Anketa urađena: " + simpleDateFormat.format(datumRadaAnkete) //provjeriti tip
        else if(color == "zelena")
            return "Vrijeme zatvaranja: " + datumKrajaAnkete
        else if(color == "zuta")
            return "Vrijeme aktiviranja: " + simpleDateFormat.format(anketa.datumPocetak)
        return "Anketa zatvorena: " + datumKrajaAnkete
    }

    fun postaviAnketaTaken(anketeTaken: List<AnketaTaken>) {
        this.anketeTaken = anketeTaken
    }

    fun postaviMapuAnketaIdIstrazivanje(mapa: MutableMap<Int, Istrazivanje>) {
        this.mapaAnketaIdIstrazivanje = mapa
    }
}
