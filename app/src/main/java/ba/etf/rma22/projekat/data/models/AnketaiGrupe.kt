package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class AnketaiGrupe(
    @PrimaryKey val id: Int,
    @ColumnInfo @SerializedName("GrupaId") val grupaId : Int,
    @ColumnInfo(name="AnketumId") @SerializedName("AnketumId") val anketaId : Int,
)
