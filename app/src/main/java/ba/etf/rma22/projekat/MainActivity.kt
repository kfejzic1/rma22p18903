package ba.etf.rma22.projekat

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import ba.etf.rma22.projekat.data.repositories.MainContext
import ba.etf.rma22.projekat.view.FragmentAnkete
import ba.etf.rma22.projekat.view.FragmentIstrazivanje
import ba.etf.rma22.projekat.view.ViewPagerAdapter
import ba.etf.rma22.projekat.viewmodel.AccountViewModel


class MainActivity : AppCompatActivity() {
    private lateinit var pager : ViewPager2
    private val fragments = mutableListOf(
        FragmentAnkete.newInstance(),
        FragmentIstrazivanje.newInstance()
    )
    private val pagerAdapter = ViewPagerAdapter(supportFragmentManager, fragments, lifecycle)
    private val accountViewModel = AccountViewModel()
    private val br: BroadcastReceiver = ConnectivityBroadcastReceiver()
    private val filter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MainContext.mainContext = applicationContext

        pager = findViewById(R.id.pager)
        pager.adapter = pagerAdapter

        if(intent?.action == ACTION_VIEW)
            handleActionViewIntent(intent)
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(br, filter)
    }

    override fun onPause() {
        unregisterReceiver(br)
        super.onPause()
    }

    private fun handleActionViewIntent(ref: Intent?) {
        if (ref != null) {

            ref.getStringExtra("payload")?.let {
                accountViewModel.postaviHash(it, onSuccess = ::onSuccess, onError = ::onError)
            }
        }
    }

    fun updateFragmentOnPosition(fragment : Fragment, position : Int) {
        pagerAdapter.refreshFragment(position, fragment)
    }

    private fun onSuccess() {
        println("Uspjesno postavljen hash!")
    }

    private fun onError() {
        println("Neuspjesno postavljen hash!")
    }

    fun addFragmentOnPosition(fragment : Fragment, position: Int) {
        pagerAdapter.add(position, fragment)
    }

    fun setCurrentFragment(position : Int, swipe : Boolean) {
        pager.setCurrentItem(position, swipe)
    }

    fun removeAllFragments() {
        pagerAdapter.clearAllFragments()
    }

    fun getNumberOfFragments() : Int {
        return pagerAdapter.itemCount
    }
}