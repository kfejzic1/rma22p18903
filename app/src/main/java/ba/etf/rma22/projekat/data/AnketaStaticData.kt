package ba.etf.rma22.projekat.data

import ba.etf.rma22.projekat.data.models.Anketa
import java.util.*

val calPocetak: Calendar = Calendar.getInstance()
val calKraj: Calendar = Calendar.getInstance()
val calRad: Calendar = Calendar.getInstance()

fun allAnkete() : List<Anketa> {
    val ankete : ArrayList<Anketa> = ArrayList()

//    calPocetak.set(2023,5,1)
//    calKraj.set(2023,6,1)
//    ankete.add(Anketa("Druga buduća anketa", "Drugo istraživanje", calPocetak.time, calKraj.time, null, 80, "Četvrta grupa", 0F))
//
//    calPocetak.set(2022,5,1)
//    calKraj.set(2022,6,1)
//    ankete.add(Anketa("Treća anketa", "Treće istraživanje", calPocetak.time, calKraj.time, null, 80, "Peta grupa", 0F))
//
//    calPocetak.set(2022,5,1)
//    calKraj.set(2022,6,1)
//    ankete.add(Anketa("Buduća nova anketa", "Treće istraživanje", calPocetak.time, calKraj.time, null, 80, "Šesta grupa", 0F))
//
//    calPocetak.set(2022,5,1)
//    calKraj.set(2022,6,1)
//    ankete.add(Anketa("Buduća anketa", "Četvrto istraživanje", calPocetak.time, calKraj.time, null, 80, "Osma grupa", 0F))
//
//    calPocetak.set(2023,5,1)
//    calKraj.set(2023,6,1)
//    ankete.add(Anketa("Anketica", "Peto istraživanje", calPocetak.time, calKraj.time, null, 80, "Deseta grupa", 0F))
//
//    calPocetak.set(2023,5,1)
//    calKraj.set(2023,6,1)
//    ankete.add(Anketa("Survey", "Sedmo istraživanje", calPocetak.time, calKraj.time, null, 80, "Trinaesta grupa", 0F))
//
//    calPocetak.set(2023,5,1)
//    calKraj.set(2023,6,1)
//    ankete.add(Anketa("Survey anketica", "Osmo istraživanje", calPocetak.time, calKraj.time, null, 80, "Petnaesta grupa", 0F))
//
//    calPocetak.set(2020,1,1)
//    calKraj.set(2020,5,1)
//    ankete.add(Anketa("Anketa koja nije aktivna niti urađena", "Prvo istraživanje", calPocetak.time, calKraj.time, null, 200, "Druga grupa", 0F))
//
//    calPocetak.set(2020,1,1)
//    calKraj.set(2020,5,1)
//    ankete.add(Anketa("Anketa koja nije aktivna niti urađena br. 2", "Prvo istraživanje", calPocetak.time, calKraj.time, null, 200, "Nova grupa", 0F))
//
//    calPocetak.set(2019,1,1)
//    calKraj.set(2019,5,1)
//    ankete.add(Anketa("Neaktivna, neurađena anketa", "Treće istraživanje", calPocetak.time, calKraj.time, null, 200, "Najnovija grupa", 0F))
//
//    calPocetak.set(2021,3,10)
//    calKraj.set(2021,5,3)
//    calRad.set(2021,4,1)
//    ankete.add(Anketa("Zavrsena prva anketa", "Prvo istraživanje", calPocetak.time, calKraj.time, calRad.time, 100, "Prva grupa", 1F))
//
//    calPocetak.set(2021,3,10)
//    calKraj.set(2021,5,3)
//    calRad.set(2021,4,1)
//    ankete.add(Anketa("Još jedna prva anketa", "Drugo istraživanje", calPocetak.time, calKraj.time, calRad.time, 100, "Treća grupa", 1F))
//
//    calPocetak.set(2021,3,10)
//    calKraj.set(2021,5,3)
//    calRad.set(2021,4,1)
//    ankete.add(Anketa("Neka anketa", "Četvrto istraživanje", calPocetak.time, calKraj.time, calRad.time, 100, "Sedma grupa", 1F))
//
//    calPocetak.set(2021,3,10)
//    calKraj.set(2021,5,3)
//    calRad.set(2021,4,1)
//    ankete.add(Anketa("Još jedna završena", "Šesto istraživanje", calPocetak.time, calKraj.time, calRad.time, 100, "Jedanaesta grupa", 1F))
//
//    calPocetak.set(2022,3,10)
//    calKraj.set(2022,3,20)
//    calRad.set(2022,3,15)
//    ankete.add(Anketa("Zavrsena druga anketa", "Peto istraživanje", calPocetak.time, calKraj.time, calRad.time, 100, "Deveta grupa", 1F))
//
//    calPocetak.set(2022,3,10)
//    calKraj.set(2022,3,20)
//    calRad.set(2022,3,15)
//    ankete.add(Anketa("Elephant survey", "Sedmo istraživanje", calPocetak.time, calKraj.time, calRad.time, 100, "Tim grupa", 1F))
//
//    calPocetak.set(2022,1,1)
//    calKraj.set(2022,5,1)
//    ankete.add(Anketa("Aktivna, neurađena anketa", "Šesto istraživanje", calPocetak.time, calKraj.time, null, 200, "Dvanaesta grupa", 0F))
//
//    calPocetak.set(2022,3,1)
//    calKraj.set(2022,6,1)
//    ankete.add(Anketa("Anketa anketica", "Četvrto istraživanje", calPocetak.time, calKraj.time, null, 200, "Najnajnovija grupa", 0.8F))
//
//    calPocetak.set(2022,3,1)
//    calKraj.set(2022,6,1)
//    ankete.add(Anketa("Monkey survey", "Sedmo istraživanje", calPocetak.time, calKraj.time, null, 200, "Četrnaesta grupa", 0.8F))
//
//    calPocetak.set(2023,5,1)
//    calKraj.set(2023,6,1)
//    ankete.add(Anketa("Turtle survey", "Osmo istraživanje", calPocetak.time, calKraj.time, null, 80, "Šesnaesta grupa", 0F))

    return ankete
}
