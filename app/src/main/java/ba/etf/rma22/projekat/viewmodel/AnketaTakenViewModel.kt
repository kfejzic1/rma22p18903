package ba.etf.rma22.projekat.viewmodel

import android.content.Context
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.repositories.TakeAnketaRepository
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class AnketaTakenViewModel {
    val scope = MainScope()

    fun getPoceteAnkete(onSuccess: (ankete: List<AnketaTaken>) -> Unit, onError: () -> Unit) {
        scope.launch {
            val result = TakeAnketaRepository.getPoceteAnkete()
            println("Dobijam poceteAnkete")
            when(result) {
                is List<AnketaTaken> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun zapocniAnketu(idAnkete: Int, onSuccess: (anketaTaken : AnketaTaken) -> Unit, onError: () -> Unit) {
        scope.launch {
            val result = TakeAnketaRepository.zapocniAnketu(idAnkete)
            if(result == null) onError.invoke()
            when(result) {
                is AnketaTaken -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }
}