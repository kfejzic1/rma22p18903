package ba.etf.rma22.projekat.viewmodel

import android.content.Context
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.GrupaByAnketa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class GrupaViewModel {
    val scope = MainScope()

    fun getGrupeZaIstrazivanje(idIstrazivanja: Int, onSuccess: (ankete: List<Grupa>) -> Unit, onError: () -> Unit) {
        scope.launch {
            when(val result = IstrazivanjeIGrupaRepository.getGrupeZaIstrazivanje(idIstrazivanja)) {
                is List<Grupa> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun getUpisaneGrupe(onSuccess: (grupe: List<Grupa>) -> Unit, onError: () -> Unit) {
        scope.launch {
            val result = IstrazivanjeIGrupaRepository.getUpisaneGrupe()
            when(result) {
                is List<Grupa> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }

    fun upisiUGrupu(idGrupa: Int, onSuccess: () -> Unit, onError: () -> Unit) { //Implementirati za response
        scope.launch {
            val result = IstrazivanjeIGrupaRepository.upisiUGrupu(idGrupa)
            when(result) {
                is Boolean -> onSuccess.invoke()
                else -> onError.invoke()
            }
        }
    }

    fun getGroupsByManyAnkete(ankete: List<Anketa>, onSuccess: (anketaIdIstrazivanje: MutableMap<Int, Istrazivanje>) -> Unit, onError: () -> Unit) {
        scope.launch {
            val grupaIzAnketa = mutableListOf<GrupaByAnketa>()
            for(a in ankete) {
                grupaIzAnketa.addAll(IstrazivanjeIGrupaRepository.getGrupeByAnketa(a.id))
            }

            val grupe = IstrazivanjeIGrupaRepository.getGrupe()

            val anketaIstrazivanje = mutableMapOf<Int, Int>()
            for(g in grupaIzAnketa) {
                if(grupe.map { it.id }.contains(g.id))
                    anketaIstrazivanje.put(1, g.istrazivanjeId)
//                    anketaIstrazivanje.put(g.anketaIGrupe.anketaId, g.istrazivanjeId)
            }

            val result = mutableMapOf<Int, Istrazivanje>()
            for(x in anketaIstrazivanje) {
                result.put(x.key, IstrazivanjeIGrupaRepository.getIstrazivanjeById(x.value))
            }

            when(result) {
                is MutableMap<Int, Istrazivanje> -> onSuccess.invoke(result)
                else -> onError.invoke()
            }
        }
    }
}