package ba.etf.rma22.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.OdgovorPost
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.viewmodel.OdgovorViewModel
import ba.etf.rma22.projekat.viewmodel.PitanjeAnketaViewModel


class FragmentPitanje : Fragment() {
    private lateinit var tekstPitanja : TextView
    private lateinit var odgovoriLista : ListView
    private lateinit var dugmeZaustavi : Button
    private lateinit var pitanje : Pitanje
    private lateinit var anketaTaken : AnketaTaken
    private var pozicijaOdgovora : Int? = -1
    private var odgovorPost = OdgovorPost(null, null, null)
    private var odgovorViewModel = OdgovorViewModel()
    private var pitanjeAnketaViewModel = PitanjeAnketaViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_pitanje, container, false)

        tekstPitanja = view.findViewById(R.id.tekstPitanja)
        odgovoriLista = view.findViewById(R.id.odgovoriLista)
        dugmeZaustavi = view.findViewById(R.id.dugmeZaustavi)

        tekstPitanja.text = pitanje.tekstPitanja

//        if(anketa.datumKraj < Date() || anketa.datumRada!=null)   //Onemogući mijenjanje odgovora na prošloj anketi
//            odgovoriLista.isEnabled = false

        //pozicijaOdgovora = pitanjeAnketaViewModel.getAnswerPositionFromAnketa(pitanje, anketa)
        val adapter: ArrayAdapter<String> = object : ArrayAdapter<String>(requireActivity(), R.layout.odgovori_item, pitanje.opcije) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val v = super.getView(position, convertView, parent)
                val tv = v.findViewById<TextView>(android.R.id.text1)
                if(pozicijaOdgovora == position) {
                    tv.setTextColor(Color.parseColor("#0000FF"))
                }
                return v
            }
        }
        odgovoriLista.adapter = adapter

        odgovoriLista.onItemClickListener = OnItemClickListener { parent, _, position, id ->
            //Omogućen odabir jednog odgovora
            for(i in pitanje.opcije.indices) {
                val prevSelected = parent.getChildAt(i) as TextView
                prevSelected.setTextColor(ContextCompat.getColor(view.context, R.color.txtColor))
            }
            val selected = parent.getChildAt(position) as TextView
            selected.setTextColor(Color.parseColor("#0000FF"))
            pozicijaOdgovora = position

            if(odgovorPost.pozicijaOdgovora == null) {
                val act = (activity as MainActivity)
                val trenutniProgres = odgovorViewModel.getProgres()
                if(trenutniProgres != 1F) {
                    odgovorViewModel.updateProgres(trenutniProgres + 1F/(act.getNumberOfFragments()-1))
                }
            }

            odgovorPost = OdgovorPost(position, pitanje.id, 0.0f)

//            pitanjeAnketaViewModel.addAnswerToQuestionFromAnkete(this.pitanje.naziv, this.anketa.naziv, this.anketa.nazivIstrazivanja, pozicijaOdgovora)
        }

        dugmeZaustavi.setOnClickListener {
            //Potrebno je vratiti se na listu anketa i odraditi update progresa
            val act = (activity as MainActivity)

            act.removeAllFragments()
            act.addFragmentOnPosition(FragmentAnkete.newInstance(), 0)
            act.addFragmentOnPosition(FragmentIstrazivanje.newInstance(), 1)
            act.setCurrentFragment(0, false)
            //Spremi odgovor

//            val brojPitanja = pitanjeAnketaViewModel.getNumberOfQuestions(anketa)
//            val brojOdgovorenihPitanja = pitanjeAnketaViewModel.getNumberOfAnsweredQuestions(anketa)
//            val output = updateProgressWithStep2(brojOdgovorenihPitanja.toFloat() / brojPitanja)
//            anketa.progres = output/100F

//            pitanjeAnketaViewModel.addAnswerToQuestionFromAnkete(this.pitanje.naziv, this.anketa.naziv, this.anketa.nazivIstrazivanja, pozicijaOdgovora)
        }

        return view
    }

    override fun onPause() {
        super.onPause()
        odgovorPost.pozicijaOdgovora?.let {
            odgovorViewModel.postaviOdgovorAnketa(anketaTaken.id, pitanje.id, it, onSuccess = ::onSuccessAddedAnswer, onError = ::onError)
        }
    }

    fun onError() {
        val toast = Toast.makeText(context, "Search error", Toast.LENGTH_SHORT)
        toast.show()
    }

    fun onSuccessAddedAnswer() {
        println("Dodan odgovor")
        println("AnketaTaken id je: " + this.anketaTaken.id)

        //Potrebno prikazati grešku i odraditi update progresa
    }

    companion object {
        fun newInstance(pitanje : Pitanje, anketaTaken: AnketaTaken) : FragmentPitanje = FragmentPitanje().apply {
            this.pitanje = pitanje
            this.anketaTaken = anketaTaken
        }
    }
}