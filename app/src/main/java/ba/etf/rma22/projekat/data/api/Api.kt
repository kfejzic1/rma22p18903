package ba.etf.rma22.projekat.data.api

import ba.etf.rma22.projekat.data.models.*
import ba.etf.rma22.projekat.data.repositories.AccountRepository
import retrofit2.Response
import retrofit2.http.*

interface Api {
    @GET("/anketa/{id}/pitanja")
        suspend fun getPitanja(@Path("id") id: Int) : Response<List<Pitanje>>

    @POST("/student/{id}/anketa/{kid}")
        suspend fun zapocniAnketu(@Path("kid") kid: Int,
                                  @Path("id") id:String = AccountRepository.getHash()
    ) : Response<AnketaTaken>

    @POST("/student/{id}/anketataken/{kid}/odgovor")
    suspend fun postaviOdgovorAnketaTaken(@Path("kid") kid: Int,
                                          @Body odgovorPost : OdgovorPost,
                                          @Path("id") id:String = AccountRepository.getHash()
    ) : Response<PostResponse>


    @GET("/student/{id}/anketataken")
    suspend fun getPoceteAnkete(@Path("id") id:String = AccountRepository.getHash()) : Response<List<AnketaTaken>>

    @GET("/student/{id}/anketataken/{ktid}/odgovori")
        suspend fun getOdgovoriAnketa(@Path("ktid") ktid: Int,
                                      @Path("id") id:String = AccountRepository.getHash()
    ) : Response<List<Odgovor>>

    @GET("/anketa")
    suspend fun getAllAnkete(@Query("offset") offset: Int) : Response<ArrayList<Anketa>>

    @GET("/anketa/{id}")
    suspend fun getAnketaById(@Path("id") id: Int) : Response<Anketa>

    @GET("/istrazivanje")
    suspend fun getIstrazivanja(@Query("offset") offset: Int) : Response<List<Istrazivanje>>

    @GET("/istrazivanje/{id}")
    suspend fun getIstrazivanjaById(@Path("id") istrazivanjeId: Int) : Response<Istrazivanje>

    @GET("/grupa/{gid}/istrazivanje")
    suspend fun getIstrazivanjeByGrupa(@Path("gid") idGrupa: Int) : Response<Istrazivanje>

    @GET("/grupa/{id}/ankete")
    suspend fun getAnketeFromGrupa(@Path("id") idGrupa: Int) : Response<List<Anketa>>

    @GET("/anketa/{id}/grupa")
    suspend fun getGrupeByAnketa(@Path("id") anketaId: Int) : Response<List<GrupaByAnketa>>

    @GET("/grupa")
    suspend fun getGrupe() : Response<List<Grupa>>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun upisiUGrupu(@Path("gid") gid: Int,
                            @Path("id") id: String = AccountRepository.getHash(),
    ) : Response<PostResponse> //Mozda je moguce bez PostResponse, direktno sa stringom

    @GET("/student/{id}/grupa")
    suspend fun getUpisaneGrupe(@Path("id") id: String = AccountRepository.getHash()) : Response<List<Grupa>>

    @DELETE("/student/{id}/upisugrupepokusaji")
    suspend fun deleteAccountData(@Path("id") id: String = AccountRepository.getHash())
}