package ba.etf.rma22.projekat.data.models
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class OdgovorPost(
    @PrimaryKey() @SerializedName("odgovor") val pozicijaOdgovora: Int?,
    @ColumnInfo(name="pitanje") @SerializedName("pitanje") val idPitanja: Int?,
    @ColumnInfo(name="progres") @SerializedName("progres") val progres: Float?
)
