//package ba.etf.rma22.projekat
//
//import android.graphics.Color
//import android.os.Handler
//import android.os.Looper
//import androidx.recyclerview.widget.RecyclerView
//import androidx.test.espresso.Espresso.onData
//import androidx.test.espresso.Espresso.onView
//import androidx.test.espresso.action.ViewActions.click
//import androidx.test.espresso.assertion.ViewAssertions.matches
//import androidx.test.espresso.contrib.RecyclerViewActions
//import androidx.test.espresso.matcher.ViewMatchers.*
//import androidx.test.ext.junit.rules.ActivityScenarioRule
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import ba.etf.rma22.projekat.UtilTestClass.Companion.withTextColor
//import ba.etf.rma22.projekat.data.repositories.AnketaRepository
//import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
//import ba.etf.rma22.projekat.view.FragmentIstrazivanje
//import org.hamcrest.CoreMatchers.*
//import org.junit.Assert.assertEquals
//import org.junit.Assert.assertNotEquals
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.hamcrest.CoreMatchers.`is` as Is
//
//@RunWith(AndroidJUnit4::class)
//class MySpirala2AndroidTest {
//
//    @get:Rule
//    val intentsTestRule = ActivityScenarioRule<MainActivity>(MainActivity::class.java)
//
//    @Test
//    fun testUpisIstrazivanjaPoruka() {
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(1))
//        onView(withId(R.id.odabirGodina)).perform(click())
//        onData(allOf(Is(instanceOf(String::class.java)), Is("1"))).perform(click())
//        onView(withId(R.id.odabirIstrazivanja)).perform(click())
//        onData(allOf(Is(instanceOf(String::class.java)), Is("Treće istraživanje"))).perform(click())
//        onView(withId(R.id.odabirGrupa)).perform(click())
//        onData(allOf(Is(instanceOf(String::class.java)), Is("Peta grupa"))).perform(click())
//        onView(withId(R.id.dodajIstrazivanjeDugme)).perform(click())
//        onView(withSubstring("Uspješno ste upisani")).check(matches(isDisplayed()))
//
//        //Provjera da li je nulti i prvi fragment FragmentAnkete i FragmentIstrazivanje nakon skrola
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(0))
//
//        //Ako nulti fragment nije FragmentAnkete, ovdje će test pasti
//        onView(withId(R.id.filterAnketa)).check(matches(isDisplayed()))
//
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(1))
//
//        //Ako prvi fragment nije FragmentIstrazivanje, ovdje ce test pasti
//        onView(withId(R.id.odabirGodina)).check(matches(isDisplayed()))
//    }
//
//    @Test
//    fun testSpremanjeOdgovoraIProgresa() {
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(0))
//        val ankete = AnketaRepository.getMyAnkete()
//
//        onView(withId(R.id.listaAnketa)).perform(   //Klik na anketa[0]
//            RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(allOf(hasDescendant(withText(ankete[0].naziv)),
//                hasDescendant(withText(ankete[0].nazivIstrazivanja))), click()))
//
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(0))
//        onData(anything()).inAdapterView(allOf(isDisplayed(), withId(R.id.odgovoriLista))).atPosition(0).perform(click()) //Klikni na prvi odgvor
//        onView(withId(R.id.dugmeZaustavi)).perform(click()) //Zaustavi nakon zadnjeg odgovora
//
//        assertNotEquals(0F, ankete[0].progres)
//
//        onView(withId(R.id.listaAnketa)).perform(   //Klik na anketa[0]
//            RecyclerViewActions.actionOnItem<RecyclerView.ViewHolder>(allOf(hasDescendant(withText(ankete[0].naziv)),
//                hasDescendant(withText(ankete[0].nazivIstrazivanja))), click()))
//
//        onView(withId(R.id.pager)).perform(ViewPager2Actions.scrollToPosition(0))
//        onData(anything()).inAdapterView(allOf(isDisplayed(), withId(R.id.odgovoriLista))).atPosition(0).check(
//            matches(withTextColor(Color.parseColor("#0000FF"))))
//    }
//}